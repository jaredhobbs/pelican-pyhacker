# PyHacker theme for Pelican

This is the theme I created for my blog at <http://pyhacker.com>. I wanted the
look and feel to mimic my terminal environment.

## Settings used by this theme (all optional, suggested values shown below)

- `DEFAULT_DATE_FORMAT = '%a %b %d %H:%M:%S %Z %Y'`
- `TAG_CLOUD_STEPS = 8`
- `SUMMARY_MAX_LENGTH = 30`
- `DISPLAY_PAGES_ON_MENU = True`
- `FOOTER_TEXT = 'Replace powered by pelican text'`
- `TWITTER_USERNAME = ''`
- `DISQUS_SITENAME = 'Put your disqus sitename here'`
- `GOOGLE_ANALYTICS = 'Put your google code here'`
- `GOOGLE_AD_CLIENT = 'Put your google adsense client code here'`
- `GOOGLE_AD_SLOT = 'Put your google ad slot here'`
- `GOOGLE_AD_WIDTH = 728`
- `GOOGLE_AD_HEIGHT = 90`

## Author

[Jared Hobbs](https://bitbucket.org/jaredhobbs)

## License

See [LICENSE](http://git.pyhacker.com/pelican-pyhacker/raw/master/LICENSE)
for details.
